# ffmpeg cheatsheet

## Basic syntax

```sh
ffmpeg -i input.mp4 output.mp4
```

## Inputs

Specify inputs with `-i`.

## Output

Specify output as a single argument. The extension is **significant** unless the format is explicitly given.

## Interactivity

* `-stdin`: Enable interaction on standard input.
* `-nostdin`: Disable interaction on standard input.
* `-y`: Overwrite target file without asking.

## Main


## Transcode

`-c[:stream specifier] codec (input/output,per-stream)` 
`-codec[:stream specifier] codec (input/output,per-stream)`

Select an encoder or a decoder for one or more streams.

Example:

```sh
ffmpeg -i INPUT -map 9 -c:v libx264 -c:a copy OUTPUT
```

encodes all video streams with libx264 and copies all audio streams.

[Stream specifier](https://ffmpeg.org/ffmpeg.html#toc-Stream-specifiers-1): Can be empty to specify all streams.

Codec can be `copy` to skip transcoding and simply copy the stream.

`-map`

([doc](https://ffmpeg.org/ffmpeg.html#toc-Advanced-options)) Designate one or more input streams as a source for the output file.

Example:

To select all video and the third audio stream from an input file:

```sh
ffmpeg -i INPUT -map 0:v -map 0:a:2 OUTPUT
```

`-ss`

Seeks into position of input file

### Video

* `-vcodec`: Alias of `-codec:v`

#### libx265

* `-x265-params`: Pass options to x265 encoder. Ex. `-x265-params tune=animation`

### Audio

* `-acodec`: Alias of `-codec:a`

## Metadata

`-metadata[:metadata_specifier] key=value (output,per-metadata)`: Set a metadata key/value pair

`-map_metadata`: Set metadata information from selected input to the next output file.

## Chapters

`-map_chapters`: Copy chapters from indexed input to the next output file.

## Attachments

* `-attach filename`: Add an attachment

## Informative

* `-v` / `-loglevel`: Set logging level (quiet, error, warning, info)
* `-codecs`: Show all codecs
* `-hide_banner`: Hide FFmpeg banner (copyright, build options, library versions)
* `-progress`: Send progress to url. Ex. send to stdout: `-progress pipe:1`

## Examples

Grab X11 display:

```sh
ffmpeg -f x11grab -video_size cif -framerate 25 -i :0.0 /tmp/out.mpg
```

## Other methods for transcoding

### opusenc

As of writing, when ffmpeg encodes to opus, it sets a constant bitrate by default. It also doesn't write album art. If desired, `opusenc` can be used instead.

```sh
opusenc -vbr input.wav output.opus
```

(Tip: if the input file format is not a valid format opusenc accepts, transcode it to WAV first using ffmpeg.)

### WebP

Note: ffmpeg has webp support.

* Encode: `cwebp input.png -o output.webp`
* Decode to PNG: `dwebp input.webp -o output.png`
